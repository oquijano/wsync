#lang scribble/manual
@require[@for-label[racket/base
		    (only-in racket/tcp listen-port-number?)
		    ]]

@title{wsync}
@author{Oscar Alberto Quijano Xacur}

@section{Introduction}

This package provides the server side of an API to make the server
time in milliseconds available to clients.

This is useful for synchronizing actions on different computers.

The functioning is inspired by
@hyperlink["https://github.com/NodeGuy/server-date"]{server-date} for
@hyperlink["https://nodejs.org/en"]{Node.js}, with two important
differences:


@itemlist[

@item{@hyperlink["https://en.wikipedia.org/wiki/WebSocket"]{Websockets}
are used instead of http connections.}

@item{The server time is provided in milliseconds instead of seconds.}

]

@section{Installation}

In order to install wsync you need to have
@hyperlink["https://racket-lang.org/"]{racket} installed in your
computer.


To install wsync you can use the @racket[raco] command that comes
with racket:

@verbatim|{
----------

   raco pkg install https://gitlab.com/oquijano/wsync.git

----------}|

After installing, in order to run the @racket[wsync] command, you need
to add the racket packages bin folder to your PATH environment
variable. The following command gives you the path to that folder:

@verbatim|{
----------

   racket -e "(path->string (build-path (find-system-path 'addon-dir) (version) \"bin\"))"

----------}|

You can see the built-in help for the wsync command with

@verbatim|{
----------

   wsync -h

----------}|

@section{wsync command}

The wsync command has one mandatory argument which is the port number
to listen on. For example, the following command starts listening for
connections on port 5000.

@verbatim|{
----------

   wsync 5000

----------}|

By default wsync only listens to connections from the localhost, if
you would like to change that, you can use the option --host, which
takes the ip of the device you want to listen on. If you want to
listen on all network devices you can use the value '0.0.0.0'. The
following command accepts connections from all devices on port 5000.

@verbatim|{
----------

   wsync --host 0.0.0.0 5000

----------}|



@section{wsync library}

If you want to run a server programmatically with racket you can use
the server module from the wsync package, which only provides one
function: @racket[wsync].

@defmodule[wsync/server]


@defproc[(wsync [port listen-port-number?]
		[listen-ip (or/c string? #f) #f])
		(-> any)]{

  Starts the @racket[wsync] API server listening on ip
  @racket[listen-ip] and port @racket[port]. @racket[listen-ip] should
  be the ip of the device one wants to listen for connections. If it
  is @racket[#f] connections from all devices are accepted.

  Once a connection is established to the server the only kind of
  message it accepts is a string containing a single number that
  should correspond to the current date in milliseconds of the
  client. The server answers back with a list converted to json
  containing the number received and the current date in milliseconds
  of the server.

  It returns a function that when called stops listening on the
  provided port.
}

@section{How to use the API}

The server only accepts Websocket connections to the base url (this
is, "/"). Once the connection is started it only responds to messages
consisting of a single number which should be a number that
corresponds to current client epoch time in milliseconds. This is, the
number of milliseconds elapsed since the midnight at the beginning of
January 1st, 1970, UTC. Any other kind of message is ignored.

The server responds with a json string containing a vector with two
elements. The first element is the number originally received form the
client and the second one is the server epoch time in milliseconds.

@section{Clients and public servers}

Currently there exists only one client written in JavaScript.

@itemlist[

@item{JavaScript client code:
@url{https://gitlab.com/oquijano/wsync-js-client}}

@item{JavaScript client documentation:
@url{https://oquijano.gitlab.io/wsync-js-client/}}

]

There exists also a public server running wsync that anyone can use to
synchronize their events at @url{https://wsync.net}.

@; LocalWords:  Websocket synchronizer ip json wsync localhost
@; LocalWords:  programmatically
